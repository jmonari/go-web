package main

import (
	"fmt"
	"main/pkg/handlers"
	"net/http"
)

const portNumber = ":8080"

func main() {
	http.HandleFunc("/", handlers.Home)
	http.HandleFunc("/About", handlers.About)
	fmt.Println(fmt.Sprintf("Starting application at port number %s", portNumber))
	_ = http.ListenAndServe(portNumber, nil)
}
